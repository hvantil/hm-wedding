const faunadb = require('faunadb')
const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNA_SECRET
})

if (process.argv[2]) {
  switch (process.argv[2].toLowerCase()) {
    case 'create':
      createRsvpCollection()
        .then(createIndexAllRsvp)
        .then(createIndexAllNames)
        .then(createIndexPartyByName)
        .catch(console.error)
      break
    case 'delete':
      deleteRsvpCollection()
        .catch(console.error)
      break
    default:
      console.error('invalid argument: enter "create" or "delete"')
  }
} else {
  console.error('no argument given: enter "create" or "delete"')
}

function createRsvpCollection() {
  return executeQuery(
    'creating RSVPs collection...',
    q.CreateCollection({ name: 'rsvp' })
  )
}

function createIndexAllRsvp() {
  return executeQuery(
    'creating "all_rsvp" index...',
    q.CreateIndex({
      name: 'all_rsvp',
      source: q.Collection('rsvp')
    })
  )
}

function createIndexAllNames() {
  return executeQuery(
    'creating "all_names" index...',
    q.CreateIndex({
      name: 'all_names',
      source: q.Collection('rsvp'),
      values: [
        { field: ['data', 'party', 'name'] }
      ],
      unique: true
    })
  )
}

function createIndexPartyByName() {
  return executeQuery(
    'creating "party_by_name" index...',
    q.CreateIndex({
      name: 'party_by_name',
      source: q.Collection('rsvp'),
      terms: [
        { field: ['data', 'party', 'name'] }
      ]
    })
  )
}

function deleteRsvpCollection() {
  return executeQuery(
    'deleting RSVPs collection and all associated indexes...',
    q.Delete(q.Collection('rsvp'))
  )
}

function executeQuery(message, query) {
  console.info('\n' + message)
  return client.query(query)
    .then(console.info)
}
