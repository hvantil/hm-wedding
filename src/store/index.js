import faunaClient, { deleteAllRsvps, createRsvpDocumentsFrom } from '@/utils/faunaClient'

export const state = () => ({
  allNames: [],
  ref: null,
  party: [],
})

export const mutations = {
  SET_ALL_NAMES(state, allNames) {
    state.allNames = allNames
  },
  SET_REF(state, ref) {
    state.ref = ref
  },
  SET_PARTY(state, party) {
    state.party = party
  },
}

export const actions = {
  fetchAllNames({ commit }) {
    faunaClient.getAllNames()
      .then(names => commit('SET_ALL_NAMES', names))
  },
  async fetchParty({ commit }, name) {
    const { ref, party } = await faunaClient.getParty(name)
    commit('SET_REF', ref)
    commit('SET_PARTY', party)
  },
  clearRef({ commit }) {
    commit('SET_REF', null)
  },
  async saveResponses({ commit, state }, payload) {
    commit('SET_PARTY', state.party.map(({ name, attendance, meal }) => ({ 
      name, 
      attending: payload.attendances[name],
      meal: payload.meals[name] || 'TBD'
    })))
    await faunaClient.updateParty(state.ref, { party: state.party })
  },
  getAllParties() {
    return faunaClient.getAllRsvps()
      .then(rsvps => rsvps.map(doc => doc.data.party))
  },
  overwriteRsvpCollectionWith({}, parties) {
    return deleteAllRsvps()
      .then(() => createRsvpDocumentsFrom(parties))
  }
}
