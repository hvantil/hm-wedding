export function stripAndLowercase(string) {
  const allWhitespace = /\s/g
  return string.replace(allWhitespace, '').toLowerCase()
}