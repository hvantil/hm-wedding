import faunaClient from '@/utils/faunaClient'
import csvUtils from '@/utils/csvUtils'

const endpoint = 'https://gitlab.com/api/v4/projects/15511152/repository/commits'
const authHeader = 'Bearer SzxDeYDzVbyabtSdiGSG'
const filePath = 'rsvp/guest_list.csv'
const oneHourInMillis = 3600000

export function commitRsvpCsv(csvContent, commitMessage) {
  const postParams = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: authHeader
    },
    body: commitRsvpCsvBody(csvContent, commitMessage)
  }
  return fetch(endpoint, postParams)
}

function commitRsvpCsvBody(content, commitMessage) {
  return JSON.stringify({
    branch: process.env.RSVP_BACKUP_BRANCH,
    commit_message: commitMessage || 'backup triggered via admin page',
    actions: [
      {
        action: 'update',
        file_path: filePath,
        content
      }
    ]
  })
}

export async function backupCsvIfNoneInLastHour() {
  const latestBackup = await getMostRecentCsvBackupTimestamp()
  if (dateIsMoreThanOneHourAgo(latestBackup)) {
    getAllParties()
      .then(csvUtils.parsePartiesIntoCsvContent)
      .then(csvContent => commitRsvpCsv(csvContent, 'automatic backup triggered via page visit'))
  }
}

function getMostRecentCsvBackupTimestamp() {
  const headers = { Authorization: authHeader }
  const queryParams = '?ref_name=rsvp-backups-prod&path=rsvp/guest_list.csv'
  return fetch(endpoint + queryParams, { headers })
    .then(res => res.json())
    .then(commits => commits[0].authored_date)
    .then(timestamp => timestamp ? new Date(timestamp) : new Date(0))
}

function dateIsMoreThanOneHourAgo(date) {
  return (new Date() - date) > oneHourInMillis
}

function getAllParties() {
  return faunaClient.getAllRsvps()
    .then(rsvps => rsvps.map(doc => doc.data.party))
}
