import {Attending, Meal} from '@/utils/enums'

function parseCsvContentIntoParties(csv) {
  let parties = []
  let party = {party: []}
  csv.split(/\r\n|\n/).forEach(row => {
    if (row.startsWith(',')) {
      parties.push(party)
      party = {party: []}
    } else {
      let [name, attending, meal, ...rest] = row.split(',')
      if (!Attending[attending] || !Meal[meal]) {
        throw 'invalid csv row: ' + row
      }
      party.party.push({name, attending, meal})
    }
  })
  parties.push(party)
  return parties
}

function parsePartiesIntoCsvContent(parties) {
  if (parties.length < 1) {
    return ''
  }
  let rows = []
  parties.forEach(party => {
    party.forEach(person => {
      rows.push(`${person.name},${person.attending},${person.meal}`)
    })
    rows.push(',')
  })
  return rows.slice(0, -1).join('\n')
}

export default {parseCsvContentIntoParties, parsePartiesIntoCsvContent}