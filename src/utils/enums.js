export const Attending = Object.freeze({
  TBD: 'TBD',
  NO: 'NO',
  YES: 'YES',
})

export const Meal = Object.freeze({
  TBD: 'TBD',
  CHICKEN: 'CHICKEN',
  STEAK: 'STEAK',
  PASTA: 'PASTA',
})
