import faunadb, { query as q } from "faunadb"

const CLIENT = new faunadb.Client({
  secret: process.env.FAUNA_SECRET
})
const PAGE_SIZE = { size: 9999 }

function getAllNames() {
  return CLIENT.query(
    q.Paginate(q.Match(q.Index('all_names')), PAGE_SIZE)
  ).then(res => res.data)
}

async function getParty(name) {
  const result = await CLIENT.query(
    q.Get(q.Match(q.Index('party_by_name'), name))
  )
  return {
    ref: result.ref,
    party: result.data.party
  }
}

function updateParty(ref, party) {
  return CLIENT.query(
    q.Update(ref, { data: party })
  )
}

function getAllRsvps() {
  return CLIENT.query(
    q.Map(
      q.Paginate(q.Match(q.Index('all_rsvp')), PAGE_SIZE),
      q.Lambda('ref', q.Get(q.Var('ref')))
    )
  ).then(res => res.data)
}

export default { getAllNames, getParty, updateParty, getAllRsvps }

export function deleteAllRsvps() {
  return CLIENT.query(
    q.Map(
      q.Paginate(q.Match(q.Index('all_rsvp')), PAGE_SIZE),
      q.Lambda('ref', q.Delete(q.Var('ref')))
    )
  )
}

export function createRsvpDocumentsFrom(parties) {
  return CLIENT.query(
    q.Map(
      parties,
      q.Lambda('data', q.Create(q.Collection('rsvp'), {data: q.Var('data')}))
    )
  )
}
