export default {
  srcDir: 'src/',
  buildDir: 'target/.nuxt',
  target: 'static',
  generate: {
    dir: 'target/app',
  },
  env: {
    FAUNA_SECRET: process.env.FAUNA_SECRET,
    RSVP_BACKUP_BRANCH: process.env.RSVP_BACKUP_BRANCH || 'rsvp-backups-dev',
  },
  css: [
    '@/assets/styles/colors.css',
    '@/assets/styles/fonts.css',
  ],
  head: {
    title: 'H&M Wedding',
    link: [
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' },
      { rel: 'manifest', href: '/favicons/site.webmanifest' },
      { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#000000' },
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'theme-color', content: '#ffffff' },
    ],
  },
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  modules: [
    'bootstrap-vue/nuxt',
  ],
  build: {
    babel: {
      plugins: [['@babel/plugin-proposal-private-methods', { loose: true }]],
    },
  },
  vuetify: {
    defaultAssets: {
      icons: 'md',
    },
    treeShake: true,
  },
}
